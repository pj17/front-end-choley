import axios from 'axios'

export default {
  getTrainings(params) {
    return axios.get('/training', {
      params
    })
  },
  editTraining(id, payload) {
    return axios.patch(`/training/${id}`, payload)
  },
  saveTraining(payload) {
    return axios.post('/training/', payload)
  },
  deleteTraining(id) {
    return axios.delete(`/training/${id}`)
  }
}
