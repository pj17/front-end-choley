import axios from 'axios'

export default {
  getUserTraining(id) {
    return axios.get(`/training/user/${id}`)
  }
}
