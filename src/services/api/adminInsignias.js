import axios from 'axios'

export default {
  getInsignias(params) {
    return axios.get('/libusers/allinsignia', {
      params
    })
  }
}
