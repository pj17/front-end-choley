import axios from 'axios'

export default {
  getLeaves(params) {
    return axios.get('/leave', {
      params
    })
  },
  getUserLeaves(id) {
    return axios.get(`/leave/user/${id}`)
  },
  editLeave(id, payload) {
    return axios.patch(`/leave/${id}`, payload)
  },
  saveLeave(payload) {
    return axios.post('/leave/', payload)
  },
  deleteLeave(id) {
    return axios.delete(`/leave/${id}`)
  }
}
