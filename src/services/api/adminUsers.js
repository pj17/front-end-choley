import axios from 'axios'

export default {
  getUsers(params) {
    return axios.get('/libusers', {
      params
    })
  },
  editUser(id, payload) {
    return axios.patch(`/libusers/${id}`, payload)
  },
  saveUser(payload) {
    return axios.post('/libusers/', payload)
  },
  deleteUser(id) {
    return axios.delete(`/libusers/${id}`)
  },
  getAllUsers() {
    return axios.get('/libusers/all')
  }
}
