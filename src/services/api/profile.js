import axios from 'axios'

export default {
  getProfile() {
    return axios.get('/profile')
  },
  saveProfile(payload) {
    return axios.patch('/profile', payload)
  },
  createProfile(payload) {
    return axios.patch('/profile/create/', payload)
  },
  updateProfile(id, payload) {
    return axios.patch(`/profile/update/${id}`, payload)
  },
  deleteProfile(id, payload) {
    return axios.patch(`/profile/${id}`, payload)
  }
}
