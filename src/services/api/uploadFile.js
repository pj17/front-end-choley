import axios from 'axios'

export default {
  uploadFile(payload) {
    return axios.post(`/upload`, payload)
  }
}
