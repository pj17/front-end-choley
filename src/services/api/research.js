import axios from 'axios'

export default {
  getUserResearchs(id) {
    return axios.get(`/research/user/${id}`)
  },
  editUserResearch(id, payload) {
    return axios.patch(`/research/${id}`, payload)
  },
  saveUserResearch(payload) {
    return axios.post('/research/', payload)
  },
  deleteUserResearch(id) {
    return axios.delete(`/research/${id}`)
  }
}
