import axios from 'axios'

export default {
  getResearchs(params) {
    return axios.get('/research', {
      params
    })
  },
  editResearchs(id, payload) {
    return axios.patch(`/research/${id}`, payload)
  },
  saveResearchs(payload) {
    return axios.post('/research/', payload)
  },
  deleteResearchs(id) {
    return axios.delete(`/research/${id}`)
  }
}
