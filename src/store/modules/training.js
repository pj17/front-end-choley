import * as types from '@/store/mutation-types'
import api from '@/services/api/training'
import { handleError } from '@/utils/utils.js'

const getters = {
  usertrainings: (state) => state.usertrainings
}

const actions = {
  getUserTraining({ commit }, payload) {
    return new Promise((resolve, reject) => {
      api
        .getUserTraining(payload)
        .then((response) => {
          if (response.status === 200) {
            commit(types.USERTRAININGS, response.data)
            resolve()
          }
        })
        .catch((error) => {
          handleError(error, commit, reject)
        })
    })
  }
}

const mutations = {
  [types.USERTRAININGS](state, usertrainings) {
    state.usertrainings = usertrainings
  }
}

const state = {
  usertrainings: []
}

export default {
  state,
  getters,
  actions,
  mutations
}
