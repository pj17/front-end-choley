import * as types from '@/store/mutation-types'
import api from '@/services/api/adminTrainings'
import { buildSuccess, handleError } from '@/utils/utils.js'

const getters = {
  trainings: (state) => state.trainings,
  totalTrainings: (state) => state.totalTrainings
}

const actions = {
  getTrainings({ commit }, payload) {
    return new Promise((resolve, reject) => {
      api
        .getTrainings(payload)
        .then((response) => {
          if (response.status === 200) {
            commit(types.TRAININGS, response.data.docs)
            commit(types.TOTAL_TRAININGS, response.data.totalDocs)
            resolve()
          }
        })
        .catch((error) => {
          handleError(error, commit, reject)
        })
    })
  },
  saveTraining({ commit }, payload) {
    return new Promise((resolve, reject) => {
      api
        .saveTraining(payload)
        .then((response) => {
          if (response.status === 201) {
            buildSuccess(
              {
                msg: 'common.SAVED_SUCCESSFULLY'
              },
              commit,
              resolve
            )
          }
        })
        .catch((error) => {
          handleError(error, commit, reject)
        })
    })
  },
  editTraining({ commit }, payload) {
    return new Promise((resolve, reject) => {
      const data = {
        topic: payload.topic,
        startDate: payload.startDate,
        endDate: payload.endDate,
        location: payload.location,
        description: payload.description,
        file: payload.file,
        listnames: payload.listnames
      }
      api
        .editTraining(payload._id, data)
        .then((response) => {
          if (response.status === 200) {
            buildSuccess(
              {
                msg: 'common.SAVED_SUCCESSFULLY'
              },
              commit,
              resolve
            )
          }
        })
        .catch((error) => {
          handleError(error, commit, reject)
        })
    })
  },
  deleteTraining({ commit }, payload) {
    return new Promise((resolve, reject) => {
      api
        .deleteTraining(payload)
        .then((response) => {
          if (response.status === 200) {
            buildSuccess(
              {
                msg: 'common.DELETED_SUCCESSFULLY'
              },
              commit,
              resolve
            )
          }
        })
        .catch((error) => {
          handleError(error, commit, reject)
        })
    })
  }
}

const mutations = {
  [types.TRAININGS](state, trainings) {
    state.trainings = trainings
  },
  [types.TOTAL_TRAININGS](state, value) {
    state.totalTrainings = value
  }
}

const state = {
  trainings: [],
  totalTrainings: 0
}

export default {
  state,
  getters,
  actions,
  mutations
}
