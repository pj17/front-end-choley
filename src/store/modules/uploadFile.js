import * as types from '@/store/mutation-types'
import api from '@/services/api/uploadFile'

import { buildSuccess, handleError } from '@/utils/utils.js'

const getters = {
  files: (state) => state.files,
  totalfiles: (state) => state.totalfiles
}

const actions = {
  uploadFile({ commit }, payload) {
    const formData = new FormData()
    formData.append('file', payload)
    return new Promise((resolve, reject) => {
      api
        .uploadFile(formData)
        .then((response) => {
          if (response.status === 200) {
            buildSuccess(
              {
                msg: 'common.SAVED_SUCCESSFULLY'
              },
              commit,
              resolve
            )
          }
        })
        .catch((error) => {
          handleError(error, commit, reject)
        })
    })
  }
}

const mutations = {
  [types.FILES](state, files) {
    state.files = files
  },
  [types.TOTAL_FILES](state, value) {
    state.totalfiles = value
  }
}

const state = {
  files: [],
  totalfiles: 0
}

export default {
  state,
  getters,
  actions,
  mutations
}
