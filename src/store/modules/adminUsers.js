import * as types from '@/store/mutation-types'
import api from '@/services/api/adminUsers'
import { buildSuccess, handleError } from '@/utils/utils.js'

const getters = {
  users: (state) => state.users,
  totalUsers: (state) => state.totalUsers
}

const actions = {
  getUsers({ commit }, payload) {
    return new Promise((resolve, reject) => {
      api
        .getUsers(payload)
        .then((response) => {
          if (response.status === 200) {
            commit(types.USERS, response.data.docs)
            commit(types.TOTAL_USERS, response.data.totalDocs)
            resolve()
          }
        })
        .catch((error) => {
          handleError(error, commit, reject)
        })
    })
  },
  editUser({ commit }, payload) {
    return new Promise((resolve, reject) => {
      const data = {
        prefix: payload.prefix,
        prefixEN: payload.prefixEN,
        nameTH: payload.nameTH,
        surnameTH: payload.surnameTH,
        nameEN: payload.nameEN,
        surnameEN: payload.surnameEN,
        // eslint-disable-next-line
        fullname: payload.nameTH + ' ' + payload.surnameTH,
        birthdate: payload.birthdate,
        idCard: payload.idCard,
        role: payload.role,
        email: payload.email,
        password: payload.password,
        serviceDay: payload.serviceDay,
        sixtyDay: payload.sixtyDay,
        retirementDay: payload.retirementDay,
        fiscalYear: payload.fiscalYear,
        ratenumber: payload.ratenumber,
        position: payload.position,
        division: payload.division,
        affiliate: payload.affiliate,
        campus: payload.campus,
        work: payload.work,
        type: payload.type,
        secondType: payload.secondType,
        appointDay: payload.appointDay,
        phone: payload.phone,
        address: payload.address,
        blood: payload.blood,
        height: payload.height,
        weight: payload.weight,
        nationality: payload.nationality,
        ethnicity: payload.ethnicity,
        religion: payload.religion,
        status: payload.status,
        educations: payload.educations,
        salarys: payload.salarys,
        historys: payload.historys,
        insignias: payload.insignias
      }
      api
        .editUser(payload._id, data)
        .then((response) => {
          if (response.status === 200) {
            buildSuccess(
              {
                msg: 'common.SAVED_SUCCESSFULLY'
              },
              commit,
              resolve
            )
          }
        })
        .catch((error) => {
          handleError(error, commit, reject)
        })
    })
  },
  saveUser({ commit }, payload) {
    return new Promise((resolve, reject) => {
      api
        .saveUser(payload)
        .then((response) => {
          if (response.status === 201) {
            buildSuccess(
              {
                msg: 'common.SAVED_SUCCESSFULLY'
              },
              commit,
              resolve
            )
          }
        })
        .catch((error) => {
          handleError(error, commit, reject)
        })
    })
  },
  deleteUser({ commit }, payload) {
    return new Promise((resolve, reject) => {
      api
        .deleteUser(payload)
        .then((response) => {
          if (response.status === 200) {
            buildSuccess(
              {
                msg: 'common.DELETED_SUCCESSFULLY'
              },
              commit,
              resolve
            )
          }
        })
        .catch((error) => {
          handleError(error, commit, reject)
        })
    })
  }
}

const mutations = {
  [types.USERS](state, users) {
    state.users = users
  },
  [types.TOTAL_USERS](state, value) {
    state.totalUsers = value
  }
}

const state = {
  users: [],
  userseducations: [],
  usershistory: [],
  totalUsers: 0
}

export default {
  state,
  getters,
  actions,
  mutations
}
