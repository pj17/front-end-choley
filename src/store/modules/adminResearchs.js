import * as types from '@/store/mutation-types'
import api from '@/services/api/adminResearchs'
import { buildSuccess, handleError } from '@/utils/utils.js'

const getters = {
  researchs: (state) => state.researchs,
  totalResearchs: (state) => state.totalResearchs
}

const actions = {
  getResearchs({ commit }, payload) {
    return new Promise((resolve, reject) => {
      api
        .getResearchs(payload)
        .then((response) => {
          if (response.status === 200) {
            commit(types.RESEARCHS, response.data.docs)
            commit(types.TOTAL_RESEARCHS, response.data.totalDocs)
            resolve()
          }
        })
        .catch((error) => {
          handleError(error, commit, reject)
        })
    })
  },
  saveResearchs({ commit }, payload) {
    return new Promise((resolve, reject) => {
      api
        .saveResearchs(payload)
        .then((response) => {
          if (response.status === 201) {
            buildSuccess(
              {
                msg: 'common.SAVED_SUCCESSFULLY'
              },
              commit,
              resolve
            )
          }
        })
        .catch((error) => {
          handleError(error, commit, reject)
        })
    })
  },
  editResearchs({ commit }, payload) {
    return new Promise((resolve, reject) => {
      const data = {
        titleThai: payload.titleThai,
        titleEnglish: payload.titleEnglish,
        institute: payload.institute,
        work: payload.work,
        researchFund: payload.researchFund,
        type: payload.type,
        expenditure: payload.expenditure,
        yearScholarship: payload.yearScholarship,
        objective: payload.objective,
        strategy: payload.strategy,
        participants: payload.participants,
        abstract: payload.abstract,
        relatedFile: payload.relatedFile
      }
      api
        .editResearchs(payload._id, data)
        .then((response) => {
          if (response.status === 200) {
            buildSuccess(
              {
                msg: 'common.SAVED_SUCCESSFULLY'
              },
              commit,
              resolve
            )
          }
        })
        .catch((error) => {
          handleError(error, commit, reject)
        })
    })
  },
  deleteResearchs({ commit }, payload) {
    return new Promise((resolve, reject) => {
      api
        .deleteResearchs(payload)
        .then((response) => {
          if (response.status === 200) {
            buildSuccess(
              {
                msg: 'common.DELETED_SUCCESSFULLY'
              },
              commit,
              resolve
            )
          }
        })
        .catch((error) => {
          handleError(error, commit, reject)
        })
    })
  }
}

const mutations = {
  [types.RESEARCHS](state, researchs) {
    state.researchs = researchs
  },
  [types.TOTAL_RESEARCHS](state, value) {
    state.totalResearchs = value
  }
}

const state = {
  researchs: [],
  totalResearchs: 0
}

export default {
  state,
  getters,
  actions,
  mutations
}
