import * as types from '@/store/mutation-types'
import api from '@/services/api/profile'
import { buildSuccess, handleError } from '@/utils/utils.js'

const getters = {
  userInsignia: (state) => state.userInsignia
}

const actions = {
  getProfile({ commit }) {
    return new Promise((resolve, reject) => {
      commit(types.SHOW_LOADING, true)
      api
        .getProfile()
        .then((response) => {
          if (response.status === 200) {
            if (
              response.data.type === 'พนักงานมหาวิทยาลัย' &&
              response.data.secondType === 'งบแผ่นดิน'
            ) {
              const insignia = []
              const array = response.data.insignias
              array.forEach((element) => {
                insignia.push({
                  year: element.year,
                  name: element.name,
                  condensed: element.condensed
                })
              })
              commit(types.FILL_INSIGNIA, insignia)
            }
            commit(types.FILL_USERINSIGNIA, response.data)
            buildSuccess(null, commit, resolve)
          }
        })
        .catch((error) => {
          handleError(error, commit, reject)
        })
    })
  }
}

const mutations = {
  [types.FILL_INSIGNIA](state, insignia) {
    state.userInsignia = insignia
  },
  [types.FILL_USERINSIGNIA](state, data) {
    state.fullname = data.fullname
    state.appointDay = data.appointDay
  }
}

const state = {
  userInsignia: [],
  fullname: '',
  serviceDay: ''
}

export default {
  state,
  getters,
  actions,
  mutations
}
