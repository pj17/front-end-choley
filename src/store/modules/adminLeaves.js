import * as types from '@/store/mutation-types'
import api from '@/services/api/adminLeaves'
import { buildSuccess, handleError } from '@/utils/utils.js'

const getters = {
  leaves: (state) => state.leaves,
  totalLeaves: (state) => state.totalLeaves,
  userleaves: (state) => state.userleaves
}

const actions = {
  getLeaves({ commit }, payload) {
    return new Promise((resolve, reject) => {
      api
        .getLeaves(payload)
        .then((response) => {
          if (response.status === 200) {
            commit(types.LEAVES, response.data.docs)
            commit(types.TOTAL_LEAVES, response.data.totalDocs)
            resolve()
          }
        })
        .catch((error) => {
          handleError(error, commit, reject)
        })
    })
  },
  getUserLeaves({ commit }, payload) {
    return new Promise((resolve, reject) => {
      api
        .getUserLeaves(payload)
        .then((response) => {
          if (response.status === 200) {
            commit(types.USERLEAVES, response.data)
            resolve()
          }
        })
        .catch((error) => {
          handleError(error, commit, reject)
        })
    })
  },
  saveLeave({ commit }, payload) {
    return new Promise((resolve, reject) => {
      api
        .saveLeave(payload)
        .then((response) => {
          if (response.status === 201) {
            buildSuccess(
              {
                msg: 'common.SAVED_SUCCESSFULLY'
              },
              commit,
              resolve
            )
          }
        })
        .catch((error) => {
          handleError(error, commit, reject)
        })
    })
  },
  editLeave({ commit }, payload) {
    return new Promise((resolve, reject) => {
      const data = {
        fullname: payload.fullname,
        type: payload.type,
        start: payload.start,
        end: payload.end,
        quantity: payload.quantity,
        description: payload.description,
        certificate: payload.certificate,
        _foreignKey: payload._foreignKey
      }
      api
        .editLeave(payload._id, data)
        .then((response) => {
          if (response.status === 200) {
            buildSuccess(
              {
                msg: 'common.SAVED_SUCCESSFULLY'
              },
              commit,
              resolve
            )
          }
        })
        .catch((error) => {
          handleError(error, commit, reject)
        })
    })
  },
  deleteLeave({ commit }, payload) {
    return new Promise((resolve, reject) => {
      api
        .deleteLeave(payload)
        .then((response) => {
          if (response.status === 200) {
            buildSuccess(
              {
                msg: 'common.DELETED_SUCCESSFULLY'
              },
              commit,
              resolve
            )
          }
        })
        .catch((error) => {
          handleError(error, commit, reject)
        })
    })
  }
}

const mutations = {
  [types.LEAVES](state, leaves) {
    state.leaves = leaves
  },
  [types.TOTAL_LEAVES](state, value) {
    state.totalLeaves = value
  },
  [types.USERLEAVES](state, userleaves) {
    state.userleaves = userleaves
  }
}

const state = {
  leaves: [],
  totalLeaves: 0,
  userleaves: []
}

export default {
  state,
  getters,
  actions,
  mutations
}
