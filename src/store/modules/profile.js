import * as types from '@/store/mutation-types'
import api from '@/services/api/profile'
import { buildSuccess, handleError } from '@/utils/utils.js'

const getters = {
  profile: (state) => state.profile
}

const actions = {
  getProfile({ commit }) {
    return new Promise((resolve, reject) => {
      commit(types.SHOW_LOADING, true)
      api
        .getProfile()
        .then((response) => {
          if (response.status === 200) {
            commit(types.FILL_PROFILE, response.data)
            commit(types.FILL_PROFILE2, response.data)
            commit(types.FILL_PROFILE3, response.data)
            buildSuccess(null, commit, resolve)
          }
        })
        .catch((error) => {
          handleError(error, commit, reject)
        })
    })
  },
  saveProfile({ commit }, payload) {
    return new Promise((resolve, reject) => {
      commit(types.SHOW_LOADING, true)
      api
        .saveProfile(payload)
        .then((response) => {
          if (response.status === 200) {
            commit(types.FILL_PROFILE, response.data)
            buildSuccess(
              {
                msg: 'myNewProfile.PROFILE_SAVED_SUCCESSFULLY'
              },
              commit,
              resolve
            )
          }
        })
        .catch((error) => {
          handleError(error, commit, reject)
        })
    })
  },
  addProfileData({ commit }, data) {
    commit(types.ADD_PROFILE_DATA, data)
  },
  createProfile({ commit }, payload) {
    return new Promise((resolve, reject) => {
      commit(types.SHOW_LOADING, true)
      api
        .createProfile(payload)
        .then((response) => {
          if (response.status === 200) {
            commit(types.FILL_PROFILE, response.data)
            buildSuccess(
              {
                msg: 'myNewProfile.PROFILE_SAVED_SUCCESSFULLY'
              },
              commit,
              resolve
            )
          }
        })
        .catch((error) => {
          handleError(error, commit, reject)
        })
    })
  },
  updateProfile({ commit }, payload) {
    return new Promise((resolve, reject) => {
      const data = {
        educations: {
          _id: payload._id,
          name: payload.name,
          abbreviation: payload.abbreviation,
          major: payload.major,
          year: payload.year,
          institution: payload.institution
        }
      }
      api
        .updateProfile(payload.userId, data)
        .then((response) => {
          if (response.status === 200) {
            buildSuccess(
              {
                msg: 'myNewProfile.PROFILE_SAVED_SUCCESSFULLY'
              },
              commit,
              resolve
            )
          }
        })
        .catch((error) => {
          handleError(error, commit, reject)
        })
    })
  },
  deleteProfile({ commit }, payload) {
    return new Promise((resolve, reject) => {
      const data = { educations: { _id: payload.eduId } }
      api
        .deleteProfile(payload._id, data)
        .then((response) => {
          if (response.status === 200) {
            buildSuccess(
              {
                msg: 'myNewProfile.PROFILE_SAVED_SUCCESSFULLY'
              },
              commit,
              resolve
            )
          }
        })
        .catch((error) => {
          handleError(error, commit, reject)
        })
    })
  }
}

const mutations = {
  [types.FILL_PROFILE](state, data) {
    // New
    state.profile.prefix = data.prefix
    state.profile.nameTH = data.nameTH
    state.profile.surnameTH = data.surnameTH
    state.profile.birthdate = data.birthdate
    state.profile.blood = data.blood
    state.profile.height = data.height
    state.profile.weight = data.weight
    state.profile.religion = data.religion
    state.profile.status = data.status
    state.profile.nationality = data.nationality
    state.profile.ethnicity = data.ethnicity
    state.profile.idCard = data.idCard
    state.profile.address = data.address
    state.profile.serviceDay = data.serviceDay
    state.profile.sixtyDay = data.sixtyDay
  },
  [types.FILL_PROFILE2](state, data) {
    state.profile.prefixEN = data.prefixEN
    state.profile.nameEN = data.nameEN
    state.profile.surnameEN = data.surnameEN
    state.profile._id = data._id
    state.profile.retirementDay = data.retirementDay
    state.profile.fiscalYear = data.fiscalYear
    state.profile.ratenumber = data.ratenumber
    state.profile.position = data.position
    state.profile.division = data.division
    state.profile.work = data.work
    state.profile.campus = data.campus
    state.profile.affiliate = data.affiliate
    state.profile.email = data.email
    state.profile.phone = data.phone
    state.profile.educations = data.educations
  },
  [types.FILL_PROFILE3](state, data) {
    state.profile.historys = data.historys
    state.profile.salarys = data.salarys
    state.profile.insignias = data.insignias
  },
  [types.ADD_PROFILE_DATA](state, data) {
    switch (data.key) {
      case 'phone':
        state.profile.phone = data.value
        break
      case 'email':
        state.profile.email = data.value
        break
      case 'address':
        state.profile.address = data.value
        break
      default:
        break
    }
  }
}

const state = {
  profile: {
    _id: '',
    prefix: '',
    nameTH: '',
    surnameTH: '',
    prefixEN: '',
    nameEN: '',
    surnameEN: '',
    birthdate: '',
    blood: '',
    height: '',
    weight: '',
    religion: '',
    status: '',
    nationality: '',
    ethnicity: '',
    idCard: '',
    address: '',
    serviceDay: '',
    sixtyDay: '',
    retirementDay: '',
    fiscalYear: '',
    ratenumber: '',
    position: '',
    division: '',
    work: '',
    campus: '',
    affiliate: '',
    phone: '',
    email: '',
    educations: [],
    historys: [],
    salarys: [],
    insignias: []
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
