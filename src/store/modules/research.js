import * as types from '@/store/mutation-types'
import api from '@/services/api/research'
import { buildSuccess, handleError } from '@/utils/utils.js'

const getters = {
  userResearchs: (state) => state.userResearchs
}

const actions = {
  getUserResearchs({ commit }, payload) {
    return new Promise((resolve, reject) => {
      api
        .getUserResearchs(payload)
        .then((response) => {
          if (response.status === 200) {
            commit(types.USERRESEARCH, response.data)
            resolve()
          }
        })
        .catch((error) => {
          handleError(error, commit, reject)
        })
    })
  },
  saveUserResearch({ commit }, payload) {
    return new Promise((resolve, reject) => {
      api
        .saveUserResearch(payload)
        .then((response) => {
          if (response.status === 201) {
            buildSuccess(
              {
                msg: 'common.SAVED_SUCCESSFULLY'
              },
              commit,
              resolve
            )
          }
        })
        .catch((error) => {
          handleError(error, commit, reject)
        })
    })
  },
  editUserResearch({ commit }, payload) {
    return new Promise((resolve, reject) => {
      const data = {
        titleThai: payload.titleThai,
        titleEnglish: payload.titleEnglish,
        institute: payload.institute,
        work: payload.work,
        researchFund: payload.researchFund,
        type: payload.type,
        expenditure: payload.expenditure,
        yearScholarship: payload.yearScholarship,
        objective: payload.objective,
        strategy: payload.strategy,
        participants: payload.participants,
        abstract: payload.abstract,
        relatedFile: payload.relatedFile
      }
      api
        .editUserResearch(payload._id, data)
        .then((response) => {
          if (response.status === 200) {
            buildSuccess(
              {
                msg: 'common.SAVED_SUCCESSFULLY'
              },
              commit,
              resolve
            )
          }
        })
        .catch((error) => {
          handleError(error, commit, reject)
        })
    })
  },
  deleteUserResearch({ commit }, payload) {
    return new Promise((resolve, reject) => {
      api
        .deleteUserResearch(payload)
        .then((response) => {
          if (response.status === 200) {
            buildSuccess(
              {
                msg: 'common.DELETED_SUCCESSFULLY'
              },
              commit,
              resolve
            )
          }
        })
        .catch((error) => {
          handleError(error, commit, reject)
        })
    })
  }
}

const mutations = {
  [types.USERRESEARCH](state, userResearchs) {
    state.userResearchs = userResearchs
  }
}

const state = {
  userResearchs: []
}

export default {
  state,
  getters,
  actions,
  mutations
}
