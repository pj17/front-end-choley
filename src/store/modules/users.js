import * as types from '@/store/mutation-types'
import api from '@/services/api/adminUsers'
import { handleError } from '@/utils/utils.js'

const getters = {
  allUsers: (state) => state.allUsers
}

const actions = {
  getAllUsers({ commit }) {
    return new Promise((resolve, reject) => {
      api
        .getAllUsers()
        .then((response) => {
          if (response.status === 200) {
            const users = []
            const array = response.data
            array.forEach((element) => {
              users.push({
                _id: element._id,
                name: element.fullname
              })
            })
            commit(types.FILL_ALL_USERS, users)
            resolve()
          }
        })
        .catch((error) => {
          handleError(error, commit, reject)
        })
    })
  }
}

const mutations = {
  [types.FILL_ALL_USERS](state, users) {
    state.allUsers = users
  }
}

const state = {
  allUsers: []
}

export default {
  state,
  getters,
  actions,
  mutations
}
