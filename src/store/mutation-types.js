// app
export const SET_APP_VERSION = 'SET_APP_VERSION'
// locale
export const SET_LOCALE = 'SET_LOCALE'
// error
export const SHOW_ERROR = 'SHOW_ERROR'
export const ERROR = 'ERROR'
// success
export const SHOW_SUCCESS = 'SHOW_SUCCESS'
export const SUCCESS = 'SUCCESS'
// loading
export const SHOW_LOADING = 'SHOW_LOADING'
// auth
export const IS_LOGGED_IN = 'IS_LOGGED_IN'
export const SAVE_TOKEN = 'SAVE_TOKEN'
export const SAVE_USER = 'SAVE_USER'
export const LOGOUT = 'LOGOUT'
export const EMAIL_VERIFIED = 'EMAIL_VERIFIED'
export const RESET_EMAIL_SENT = 'RESET_EMAIL_SENT'
export const SHOW_CHANGE_PASSWORD_INPUTS = 'SHOW_CHANGE_PASSWORD_INPUTS'
// profile
export const FILL_PROFILE = 'FILL_PROFILE'
export const FILL_PROFILE2 = 'FILL_PROFILE2'
export const FILL_PROFILE3 = 'FILL_PROFILE3'
export const ADD_PROFILE_DATA = 'ADD_PROFILE_DATA'
// cities
export const FILL_ALL_CITIES = 'FILL_ALL_CITIES'
// Admin - Cities
export const CITIES = 'CITIES'
export const TOTAL_CITIES = 'TOTAL_CITIES'
// Admin - Users
export const USERS = 'USERS'
export const TOTAL_USERS = 'TOTAL_USERS'
// **Admin - Players
export const PLAYERS = 'PLAYERS'
export const TOTAL_PLAYERS = 'TOTAL_PLAYERS'
// **Admin - Persons
export const PERSONS = 'PERSONS'
export const TOTAL_PERSONS = 'TOTAL_PERSONS'
// Educations
export const TOTAL_EDUCATIONS = 'TOTAL_EDUCATIONS'
// Admin Leaves
export const LEAVES = 'LEAVES'
export const TOTAL_LEAVES = 'TOTAL_LEAVES'
export const USERLEAVES = 'USERLEAVES'
// users
export const FILL_ALL_USERS = 'FILL_ALL_USERS'
// Admin Training && User
export const TRAININGS = 'TRAININGS'
export const TOTAL_TRAININGS = 'TOTAL_TRAININGS'
export const USERTRAININGS = 'USERTRAININGS'

// Admin Researchs
export const RESEARCHS = 'RESEARCHS'
export const TOTAL_RESEARCHS = 'TOTAL_RESEARCHS'

// User Researchs
export const USERRESEARCH = 'USERRESEARCH'

// Files
export const FILES = 'FILES'
export const TOTAL_FILES = 'TOTAL_FILES'

// Insignia
export const ADMIN_INSIGNIA = 'ADMIN_INSIGNIA'
export const FILL_INSIGNIA = 'FILL_INSIGNIA'
export const FILL_USERINSIGNIA = 'FILL_USERINSIGNIA'
