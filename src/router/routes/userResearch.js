export default [
  {
    path: '/userResearch',
    name: 'userResearch',
    meta: {
      requiresAuth: true
    },
    component: () =>
      import(/* webpackChunkName: "profile" */ '@/components/UserResearch.vue')
  }
]
