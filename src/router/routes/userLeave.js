export default [
  {
    path: '/userLeave',
    name: 'userLeave',
    meta: {
      requiresAuth: true
    },
    component: () =>
      import(/* webpackChunkName: "profile" */ '@/components/UserLeave.vue')
  }
]
