export default [
  {
    path: '/userInsignia',
    name: 'userInsignia',
    meta: {
      requiresAuth: true
    },
    component: () =>
      import(/* webpackChunkName: "profile" */ '@/components/UserInsignia.vue')
  }
]
