export default [
  {
    path: '/',
    name: 'userProfile',
    component: () =>
      import(/* webpackChunkName: "landing" */ '@/components/UserProfile.vue')
  }
]
