export default [
  {
    path: '/userTraining',
    name: 'userTraining',
    meta: {
      requiresAuth: true
    },
    component: () =>
      import(/* webpackChunkName: "profile" */ '@/components/UserTraining.vue')
  }
]
