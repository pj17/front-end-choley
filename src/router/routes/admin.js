export default [
  {
    path: '/admin/personnel',
    name: 'admin-personnel',
    meta: {
      requiresAuth: true
    },
    component: () =>
      import(
        /* webpackChunkName: "admin-users" */ '@/components/AdminUsers.vue'
      )
  },
  {
    path: '/admin/leaves',
    name: 'admin-leaves',
    meta: {
      requiresAuth: true
    },
    component: () =>
      import(
        /* webpackChunkName: "admin-users" */ '@/components/AdminLeaves.vue'
      )
  },
  {
    path: '/admin/trainings',
    name: 'admin-trainings',
    meta: {
      requiresAuth: true
    },
    component: () =>
      import(
        /* webpackChunkName: "admin-users" */ '@/components/AdminTrainings.vue'
      )
  },
  {
    path: '/admin/insignias',
    name: 'admin-insignias',
    meta: {
      requiresAuth: true
    },
    component: () =>
      import(
        /* webpackChunkName: "admin-users" */ '@/components/AdminInsignias.vue'
      )
  },
  {
    path: '/admin/researches',
    name: 'admin-researches',
    meta: {
      requiresAuth: true
    },
    component: () =>
      import(
        /* webpackChunkName: "admin-users" */ '@/components/AdminResearches.vue'
      )
  }
]
