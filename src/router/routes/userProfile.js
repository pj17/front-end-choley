export default [
  {
    path: '/userProfile',
    name: 'userProfile',
    meta: {
      requiresAuth: true
    },
    component: () =>
      import(/* webpackChunkName: "profile" */ '@/components/UserProfile.vue')
  }
]
